package main

import (
	"errors"
	"fmt"
	"os"
)

func fileExist(file string) bool {
	info, err := os.Stat(file)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

type mkf struct {
	filenames []string //It can also contain a full path
}

func (p *mkf) parseArgs(args []string) error {
	if len(args) < 2 {
		return errors.New("not enough argument")
	}

	for i := range args {
		if i > 0 {
			p.filenames = append(p.filenames, args[i])
		}
	}
	return nil
}

func (p *mkf) createFile() error {
	for i := range p.filenames {
		if fileExist(p.filenames[i]) == true {
			return errors.New(p.filenames[i] + " file already exists")
		}

		tempfile, err := os.Create(p.filenames[i])
		if err != nil {
			return errors.New(p.filenames[i] + " unable to create file path doesn't exits")
		}
		tempfile.Close()
	}
	return nil
}

func main() {

	args := mkf{}
	err := args.parseArgs(os.Args)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = args.createFile()
	if err != nil {
		fmt.Println(err)
	}
}
